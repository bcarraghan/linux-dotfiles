from libqtile import layout
from settings import settings

layouts = [
  layout.Max(
    margin = settings['layouts']['margin'],
    border_normal = settings['layouts']['border_normal'],
    border_focus = settings['layouts']['border_focus'],
    border_width = settings['layouts']['border_width']
  ),
  layout.MonadTall(
    margin = settings['layouts']['margin'],
    border_normal = settings['layouts']['border_normal'],
    border_focus = settings['layouts']['border_focus'],
    border_width = settings['layouts']['border_width']
  ),
  layout.MonadWide(
    margin = settings['layouts']['margin'],
    border_normal = settings['layouts']['border_normal'],
    border_focus = settings['layouts']['border_focus'],
    border_width = settings['layouts']['border_width']
  ),
  layout.Floating(
    margin = settings['layouts']['margin'],
    border_normal = settings['layouts']['border_normal'],
    border_focus = settings['layouts']['border_focus'],
    border_width = settings['layouts']['border_width']
  ),
  layout.TreeTab(
    bg_color = settings['colors'][0],
    inactive_fg = settings['colors'][2],
    inactive_bg = settings['colors'][10],
    active_bg = settings['colors'][1],
    active_fg = settings['colors'][2],
    sections = ['Windows'],
    fontsize= 12,
    font="Roboto",
    margin_left = 2,
    section_left = 8
  ),
]

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])