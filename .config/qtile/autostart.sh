#!/bin/sh
compton --config ~/.config/compton/compton.conf &
nitrogen --restore &
urxvtd -q -o -f &
pacman -Sy &
