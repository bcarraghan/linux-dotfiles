from settings import settings
from libqtile.config import Key, Drag, Click
from libqtile.command import lazy

from groups import group_names, groups
from settings import settings

keys = [
  Key([settings['mod']], "h", lazy.layout.left()),
  Key([settings['mod']], "l", lazy.layout.right()),
  Key([settings['mod']], "j", lazy.layout.down()),
  Key([settings['mod']], "k", lazy.layout.up()),
  Key([settings['mod'], "shift"], "h", lazy.layout.swap_left()),
  Key([settings['mod'], "shift"], "l", lazy.layout.swap_right()),
  Key([settings['mod'], "shift"], "j", lazy.layout.shuffle_down()),
  Key([settings['mod'], "shift"], "k", lazy.layout.shuffle_up()),
  Key([settings['mod']], "i", lazy.layout.grow()),
  Key([settings['mod']], "m", lazy.layout.shrink()),
  Key([settings['mod']], "n", lazy.layout.normalize()),
  Key([settings['mod']], "o", lazy.layout.maximize()),
  Key([settings['mod'], "shift"], "space", lazy.layout.flip()),

  # Switch window focus to other pane(s) of stack
  Key([settings['mod']], "space", lazy.layout.next()),

  # Swap panes of split stack
  Key([settings['mod'], "shift"], "space", lazy.layout.rotate()),

  # Toggle between split and unsplit sides of stack.
  # Split = all windows displayed
  # Unsplit = 1 window displayed, like Max layout, but still with
  # multiple stack panes
  Key([settings['mod'], "shift"], "Return", lazy.layout.toggle_split()),
  Key([settings['mod']], "Return", lazy.spawn("termite")),

  # Toggle between different layouts as defined below
  Key([settings['mod']], "Tab", lazy.next_layout()),
  Key([settings['mod']], "w", lazy.window.kill()),

  Key([settings['mod'], "control"], "r", lazy.restart()),
  Key([settings['mod'], "control"], "q", lazy.shutdown()),
  Key([settings['mod'], "mod1"], "r", lazy.spawn('systemctl reboot')),
  Key([settings['mod'], "mod1"], "q", lazy.spawn('systemctl poweroff')),
  Key([settings['mod']], "r", lazy.spawn('rofi -show run')),
  # lock screen
  Key([settings['mod'], "control"], "l", lazy.spawn('i3lock -c 000000')),
]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([settings['mod']], str(i), lazy.group[name].toscreen()))          # Switch to another group
    keys.append(Key([settings['mod'], "shift"], str(i), lazy.window.togroup(name)))   # Send current window to another group

# Drag floating layouts.
mouse = [
  Drag([settings['mod']], "Button1", lazy.window.set_position_floating(),
    start=lazy.window.get_position()),
  Drag([settings['mod']], "Button3", lazy.window.set_size_floating(),
    start=lazy.window.get_size()),
  Click([settings['mod']], "Button2", lazy.window.bring_to_front())
]