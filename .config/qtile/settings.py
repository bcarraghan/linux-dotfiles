# contains global config settings

settings = {
  'mod': 'mod4',
  'terminal': 'termite',
  'colors': [
    "#272822", # panel background
    "#F92672", # background for current screen tab
    "#F8F8F2", # font color for group names
    "#645377", # background color for layout widget
    "#645377", # background for other screen tabs
    "#75715E", # dark green gradiant for other screen tabs
    "#A6E22E", # background color for network widget
    "#66D9EF", # background color for pacman widget
    "#F92672", # background color for cmus widget
    "#000000", # background color for clock widget
    "#31322f" # background color for systray widget,
  ],
  'layouts': {
    'margin': 0,
    'border_normal': '#272822',
    'border_focus': '#a6e22e',
    'border_width': 1
  }
}
