import os
import socket

from libqtile.config import Screen
from libqtile import bar, widget

from settings import settings

screens = []

def make_screen():
  return Screen(
    top=bar.Bar([
      widget.Sep(
        linewidth = 0,
        padding = 0,
        foreground = settings['colors'][2], 
        background = settings['colors'][0],
        ),
      widget.GroupBox(
        font = "Noto Sans Bold",
        fontsize = 10,
        margin_y = 0, 
        margin_x = 0, 
        padding_y = 4, 
        padding_x = 4, 
        borderwidth = 1, 
        active = settings['colors'][2], 
        inactive = settings['colors'][2],
        rounded = False,
        highlight_method = "block",
        highlight_color = settings['colors'][0],
        this_current_screen_border = settings['colors'][1],
        this_screen_border = settings['colors'] [4],
        other_current_screen_border = settings['colors'][0],
        other_screen_border = settings['colors'][0],
        urgent_border = settings['colors'][6],
        foreground = settings['colors'][2], 
        background = settings['colors'][0]
        
      ), 
      widget.Prompt(
        prompt="{0}@{1}: ".format(os.environ["USER"], socket.gethostname()), 
        font="Inconsolata",
        padding=5, 
        foreground = settings['colors'][3],
        background = settings['colors'][1]
      ),
      widget.Sep(
        linewidth = 0,
        padding = 5,
        foreground = settings['colors'][2], 
        background = settings['colors'][0]
      ),
      widget.TextBox(font="Inconsolata",
        fontsize = 10,
        foreground = '#ffd866', 
        background = settings['colors'][0],
        padding = 0,
        text = '//'
      ),
      widget.WindowName(font="Inconsolata",
        fontsize = 10,
        foreground = '#ffd866', 
        background = settings['colors'][0],
        padding = 5
      ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar06.png",
        background = settings['colors'][10]
      ),
      widget.Systray(
        background=settings['colors'][10],
        padding = 5,
        font="Noto Sans Regular"
      ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar02-b.png",
        background = settings['colors'][6]
      ),
      widget.Net(
        interface = "enp0s31f6", 
        foreground = settings['colors'][0], 
        background = settings['colors'][6],
        padding = 5,
        font="Noto Sans Regular"
      ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar03.png",
        background = settings['colors'][3]
      ),
      widget.TextBox(
        font="Sans",
        fontsize= 12,
        text=" ☵",
        foreground=settings['colors'][2], 
        background=settings['colors'][3]
      ),
      widget.CurrentLayout(
        foreground = settings['colors'][2], 
        background = settings['colors'][3],
        padding = 5,
        font="Noto Sans Regular"
      ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar04.png",
        background = settings['colors'][7]
      ),
      widget.TextBox(
        font="Sans",
        fontsize= 12,
        text=" ⟳", 
        foreground=settings['colors'][0], 
        background=settings['colors'][7]
      ),
      widget.CheckUpdates(
        distro = 'Arch',
        execute = 'None',
        update_interval = 1800,
        display_format = "{updates}",
        colour_have_updates = settings['colors'][0],
        colour_no_updates = settings['colors'][0],
        foreground = settings['colors'][0], 
        background = settings['colors'][7],
        font="Noto Sans Regular",
        padding = 5
      ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar05.png",
        background = settings['colors'][8]
      ),
      widget.TextBox(
        font="Sans",
        fontsize= 12,
        text=" ♫", 
        foreground = "d7d7d7",
        background=settings['colors'][8]
      ),
      widget.Mpris2(
        name='spotify',
        objname="org.mpris.MediaPlayer2.spotify",
        display_metadata=['xesam:title', 'xesam:artist'],
        scroll_chars=None,
        stop_pause_text='',
        font="Noto Sans Regular",
        padding = 5,
        fontsize= 12,
        foreground = "d7d7d7",
        background=settings['colors'][8]
      ),
      # widget.Cmus(
      #   max_chars = 40,
      #   update_interval = 0.5,
      #   foreground = "d7d7d7", 
      #   background = settings['colors'][8],
      #   font="Roboto"
      # ),
      widget.Image(
        scale = True,
        filename = "~/.config/qtile/images/bar07.png",
        background = settings['colors'][9]
      ),
      widget.TextBox(
        font="Sans",
        padding = 5,
        fontsize= 12,
        text=" 🕒", 
        foreground=settings['colors'][2],
        background=settings['colors'][9], 
      ),
      widget.Clock(
        foreground = settings['colors'][2], 
        background = settings['colors'][9],
        format="%Y-%m-%d %a %I:%M %p",
        font="Noto Sans Bold",
        padding = 5,
        fontsize = 12
      ),
      widget.Sep(
        linewidth = 0,
        padding = 5,
        foreground = settings['colors'][0], 
        background = settings['colors'][9]
      ),
    ],
    20,
    opacity = 1
    )
  )

def add_screen():
  screens.append(make_screen())
