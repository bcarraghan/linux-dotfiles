from libqtile.config import Group, Match

group_names = [
  (
    "dev", {
      'layout': 'treetab',
      'matches': [
        Match(wm_class=['Code'])
      ]
    }
  ),
  (
    "web", {
      'layout': 'monadtall',
      'matches': [
        Match(wm_class=['Firefox', 'Chromium'])
      ]
    }
  ),
  (
    "steam", {
      'layout': 'floating',
      'matches': [
        Match(wm_class=['Steam'])
      ]
    }
  ),
  (
    "chat", {
      'layout': 'monadtall',
      'matches': [
        Match(title=['irssi', 'Discord', 'Discord Updater'])
      ]
    }
  ),
  (
    "gfx", {
      'layout': 'max',
      'matches': [
        Match(wm_class=['Gimp'])
      ]
    }
  )
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]
