------------------------------------------------------------------------
-- Bryan Carraghan <bryan@carraghan.org>
-- http://www.gitlab.com/bcarraghan/
------------------------------------------------------------------------

-- Requires the following packages in Arch Linux:
-- xmonad xmonad-contrib xmobar vim rxvt-unicode nitrogen compton xdotool scrot dmenu

-- System.
import System.Exit (exitSuccess)

-- XMonad tiling window manager.
import XMonad

-- Window manager abstraction for virtual workspaces.
import qualified XMonad.StackSet as W

-- Utilties.
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig as EZConfig
import XMonad.Util.Run as Run

-- Hooks.
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers as ManageHelpers
import XMonad.Hooks.DynamicLog as DynamicLog
import XMonad.Hooks.ManageDocks as ManageDocks
import XMonad.Hooks.DynamicBars as DynamicBars
import XMonad.Hooks.EwmhDesktops as EwmhDesktops -- Required for xcomposite in OBS to work.

-- Actions.
import XMonad.Actions.MouseResize as MouseResize
import XMonad.Actions.CopyWindow as CopyWindow
import XMonad.Actions.WithAll as WithAll

-- Layout modifiers.
import XMonad.Layout.WindowArranger as WindowArranger
import XMonad.Layout.Renamed as Renamed
import XMonad.Layout.Spacing as Spacing
import XMonad.Layout.LimitWindows as LimitWindows

-- Layouts.
import XMonad.Layout.GridVariants as GridVariants
import XMonad.Layout.ResizableTile as ResizableTile

-- Easy configuration changes.
myModMask = mod4Mask
myTerminal = "urxvtc"
myTextEditor = "vim"
myWorkspaces = ["dev", "web", "game", "chat", "gfx"]

main =
  xmonad
  $ EwmhDesktops.ewmh
  myConfig

-- myConfig.
myConfig = defaultConfig {
  startupHook = myStartupHook,
  logHook = myLogHook,
  layoutHook = myLayoutHook,
  modMask = myModMask,
  terminal = myTerminal,
  workspaces = myWorkspacesClickable,
  borderWidth = 2,
  normalBorderColor  = "#272822",
  focusedBorderColor = "#A6E22E",
  handleEventHook = myHandleEventHook,
  manageHook = myManageHook
} `EZConfig.additionalKeysP` myKeys

-- myHandleEventHook.
myHandleEventHook = ManageDocks.docksEventHook
    -- <+> EwmhDesktops.fullscreenEventHook

-- myManageHook.
myManageHook :: ManageHook
myManageHook =
        manageSpecific
    where
        manageSpecific = composeOne
            [ ManageHelpers.isDialog -?> ManageHelpers.doCenterFloat
            , isRole =? "pop-up" -?> ManageHelpers.doCenterFloat
            , ManageHelpers.isInProperty "_NET_WM_WINDOW_TYPE"
                           "_NET_WM_WINDOW_TYPE_SPLASH" -?> ManageHelpers.doCenterFloat
            , ManageHelpers.isFullscreen -?> ManageHelpers.doFullFloat ]
        isRole = stringProperty "WM_WINDOW_ROLE"

-- myLayoutHook.
myLayoutHook = ManageDocks.avoidStruts
  $ MouseResize.mouseResize
  $ WindowArranger.windowArrange
  tall ||| grid
  where
    tall = Renamed.renamed [Replace "Tall"]
      $ LimitWindows.limitWindows 12
      $ Spacing.spacing 6
      $ ResizableTile.ResizableTall 1 (3/100) (1/2) []
    grid = Renamed.renamed [Replace "Grid"]
      $ LimitWindows.limitWindows 12
      $ Spacing.spacing 6
      $ GridVariants.Grid (16/10)

-- myStartupHook.
myStartupHook = do
  DynamicBars.dynStatusBarStartup barCreator barDestroyer
  spawnOnce "urxvtd &" 
  spawnOnce "nitrogen --restore &" 
  spawnOnce "compton --config ~/.config/compton/compton.conf &" 
  setWMName "LG3D"

-- myLogHook.
myLogHook = do
  EwmhDesktops.ewmhDesktopsLogHook
  DynamicBars.multiPP myMobarLogHookActive myMobarLogHook

-- myMobarLogHook for xmobar.
myMobarLogHook :: DynamicLog.PP
myMobarLogHook = DynamicLog.defaultPP
  { DynamicLog.ppCurrent = DynamicLog.xmobarColor "#f92672" "" . DynamicLog.wrap "[*" "]" -- Current workspace in xmobar.
  , DynamicLog.ppVisible = DynamicLog.xmobarColor "#a6e22e" "" -- Visible but not current workspace.
  , DynamicLog.ppHidden = DynamicLog.xmobarColor "#f8f8f2" "" . DynamicLog.wrap "*" "" -- Hidden workspaces in xmobar.
  , DynamicLog.ppHiddenNoWindows = DynamicLog.xmobarColor "#f8f8f2" "" -- Hidden workspaces (no windows).
  , DynamicLog.ppTitle = DynamicLog.xmobarColor "#75715e" "" . DynamicLog.shorten 80 -- Title of active window in xmobar.
  , DynamicLog.ppLayout  = DynamicLog.xmobarColor "#ae81ff" "" -- Layout.
  , DynamicLog.ppSep =  DynamicLog.pad $ DynamicLog.xmobarColor "#75715e" "" "|" -- Separators in xmobar.
  , DynamicLog.ppUrgent = DynamicLog.xmobarColor "#ffffff" "" . DynamicLog.wrap "!" "!"  -- Urgent workspace.
  , DynamicLog.ppExtras  = [gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset]                           -- # of windows current workspace
  }

-- myMobarLogHook for active screen on xmobar.
myMobarLogHookActive :: DynamicLog.PP
myMobarLogHookActive = myMobarLogHook
  { DynamicLog.ppCurrent = DynamicLog.xmobarColor "#f92672" "" . DynamicLog.wrap "[" "]"
  }

-- Create xmobar instances on screen changes.
barCreator :: DynamicBars.DynamicStatusBar
barCreator (XMonad.S sid) = Run.spawnPipe $ "xmobar -x " ++ show sid ++ " /home/bcarraghan/.xmonad/xmobar.conf"

-- Remove xmobar instances on screen changes.
barDestroyer :: DynamicBars.DynamicStatusBarCleanup
barDestroyer = return ()

-- Keybindings.
myKeys = 
  -- XMonad.
  [("M-C-r", spawn "xmonad --recompile")  -- Recompiles xmonad.
  , ("M-S-r", spawn "xmonad --restart") -- Restarts xmonad.
  , ("M-S-q", XMonad.io exitSuccess) -- Quits xmonad.

  -- Windows.
  , ("M-S-c", CopyWindow.kill1) -- Kill the currently focused client.
  , ("M-S-a", WithAll.killAll) -- Kill all the windows on current workspace.

  -- Windows navigation.
  , ("M-j", windows W.focusDown) -- Move focus to the next window.
  , ("M-k", windows W.focusUp) -- Move focus to the prev window.
  , ("M-S-j", windows W.swapDown) -- Swap the focused window with the next window.
  , ("M-S-k", windows W.swapUp) -- Swap the focused window with the prev window.

  -- Floating windows.
  , ("M-<Delete>", withFocused $ windows . W.sink) -- Push floating window back to tile.
  , ("M-S-<Delete>", WithAll.sinkAll) -- Push ALL floating windows back to tile.

  -- Layouts.
  , ("M-<Space>", sendMessage NextLayout) -- Switch to next layout.
  , ("M-C-h", sendMessage Shrink) -- Shrinks or expands window to the left.
  , ("M-C-l", sendMessage Expand) -- Shrinks or expands window to the right.
  , ("M-C-k", sendMessage MirrorShrink) -- Shrinks or expands window to the top.
  , ("M-C-j", sendMessage MirrorExpand) -- Shrinks or expands window to the bottom.

  -- Applications.
  , ("M-<Return>", spawn myTerminal)
  , ("M-r", spawn "dmenu_run -fn 'Source Code Pro:size=10' -nb '#272822' -nf '#f8f8f2' -sb '#272822' -sf '#f92672' -p 'dmenu:'")
  , ("M-b", spawn "firefox")
  , ("M-f", spawn (myTerminal ++ " -e vifm"))
  , ("<Print>", spawn "scrot ~/Pictures/Screenshots/%Y-%m-%d-%T-screenshot.png")
  , ("<XF86Calculator>", spawn "galculator")]

-- Clickable workspaces.
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]
myWorkspacesClickable = clickable . map xmobarEscape
  $ myWorkspaces
  where                                                                      
    clickable l = [ "<action=xdotool key super+" ++ show n ++ ">" ++ ws ++ "</action>" |
      (i,ws) <- zip [1..8] l,                                        
      let n = i ] 
