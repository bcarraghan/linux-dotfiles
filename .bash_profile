#
# ~/.bash_profile
#

# shell scripts from the home user bin directory
export PATH=$PATH:$HOME/bin

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Start xorg after login
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi

