# How to manage dotfile repo

1. Create an alias in your `.bashrc` file so we don't have to type out this entire command when we want to commit to this repo.
    * `alias config='/usr/bin/git --git-dir=$HOME/git_repos/linux-dotfiles.git/ --work-tree=$HOME'`
2. Clone the repo as a bare repository (I prefer to clone it in ~/git_repos)
    * `git clone --bare git@gitlab.com:bcarraghan/linux-dotfiles.git`
3. Checkout the content from the repository. This will fetch the contents (dotfiles) of the repository and place them in your home directory. If you already have some existing dotfiles there may be conflicts. You can force checkout using the `-f` flag.
    * `config checkout -f`